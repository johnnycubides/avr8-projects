#!/bin/bash -e
#						 ↑
# debug [-x -v]:[complete, abbreviated]
# Brief:	Instaldor de herramientas para crear proyectos con microcontroladores avr-8
# Author: Johnny Cubides
# e-mail: jgcubidesc@gmail.com
# date: Friday 13 May 2022
status=$?

TOOLCHAIN_PACK=avr8-gnu-toolchain-linux_x86_64
TOOLCHAIN_LINK=https://bitbucket.org/pinguinotux/toolchain-avr8/downloads/avr8-gnu-toolchain-3.6.2.1778-linux.any.x86_64.tar.gz
TOOLCHAIN_DIR=toolchain-avr8

mkdir -p $TOOLCHAIN_DIR
wget -O "toolchain-avr8/$(TOOLCHAIN_PACK).tar.gz" $TOOLCHAIN_LINK
tar xvf $TOOLCHAIN_DIR/$(TOOLCHAIN_PACK).tar.gz -C $TOOLCHAIN_DIR

sudo ln -sr $TOOLCHAIN_DIR/$TOOLCHAIN_PACK /opt/


