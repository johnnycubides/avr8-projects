#define GLUE(a, b)	a##b
#define SET_(what, p, m) GLUE(what, p) |= (1 << (m))
#define SET(what, x) SET_(what, x)
#define BS(REGISTER,PIN)	REGISTER |= (1 <<(PIN))
